

Dockerize matrix-appservice-slack for easy installs on Docker machines.
https://github.com/matrix-org/matrix-appservice-slack

Run this like:

```console
# docker run --rm -v $(pwd):/config registry.gitlab.com/eighthave/matrix-appservice-slack-docker:latest
```
