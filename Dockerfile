FROM node:boron
MAINTAINER hans@eds.org

EXPOSE 3000

VOLUME ["/config"]

RUN git clone https://github.com/matrix-org/matrix-appservice-slack && \
    cd matrix-appservice-slack && \
    npm install --only=production

ENTRYPOINT ["node"]
CMD ["app.js", "-c", "/config/config.yaml", "-p", "$MATRIX_PORT"]
